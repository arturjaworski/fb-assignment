//
//  PayloadParser.swift
//  flixbus-assignment
//
//  Created by Artur Jaworski on 14/11/2018.
//  Copyright © 2018 nomtek. All rights reserved.
//

import Foundation

typealias JSONObject = [String: Any]

class PayloadParser {

    enum Error: Swift.Error, Equatable {
        case noData
        case corruptedData
        case noResult
        case unknownResult
        case external(String)
        case unknownExternal
        case noPayload
        case unknownPayload

        var code: Int {
            switch self {
            case .noData: return 900
            case .corruptedData: return 901
            case .noResult: return 902
            case .unknownResult: return 903
            case .external(_): return 904
            case .unknownExternal: return 905
            case .noPayload: return 906
            case .unknownPayload: return 907
            }
        }

        var localizedDescription: String {
            switch self {
            case .external(let message): return message
            default:
                return "Something goes wrong (#\(self.code))"
            }
        }
    }

    func payload(from data: Data?) throws -> Any {
        guard let data = data else {
            throw Error.noData
        }

        let dataObject = try self.jsonObject(from: data)
        guard let dataDictionary = dataObject as? JSONObject, let result = dataDictionary["result"] else {
            throw Error.noResult
        }

        if try !self.isSuccess(result) {
            guard let message = dataDictionary["message"] as? String, message.count > 0 else {
                throw Error.unknownExternal
            }
            throw Error.external(message)
        }

        guard let payload = dataDictionary["payload"] else {
            // here we can return nil, but provided method definition doesn't allow to do it
            throw Error.noPayload
        }

        if let payload = payload as? [JSONObject] {
            return payload
        }

        if let payload = payload as? JSONObject, payload.count > 0 {
            return payload
        }

        throw Error.unknownPayload
    }

}

private extension PayloadParser {

    func jsonObject(from data: Data) throws -> Any {
        do {
            return try JSONSerialization.jsonObject(with: data, options: [])
        }
        catch {
            throw Error.corruptedData
        }
    }

    func isSuccess(_ result: Any) throws -> Bool {
        if let boolResult = result as? Bool {
            return boolResult
        }

        guard let stringResult = result as? String, let boolResult = Bool(stringResult) else {
            // here we can go with fallback to false if it's needed
            throw Error.unknownResult
        }

        return boolResult
    }

}
