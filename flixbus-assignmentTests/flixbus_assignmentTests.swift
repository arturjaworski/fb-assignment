//
//  flixbus_assignmentTests.swift
//  flixbus-assignmentTests
//
//  Created by Artur Jaworski on 14/11/2018.
//  Copyright © 2018 nomtek. All rights reserved.
//

import XCTest
@testable import flixbus_assignment

class flixbus_assignmentTests: XCTestCase {

    var parser: PayloadParser!

    override func setUp() {
        self.parser = PayloadParser()
    }

    override func tearDown() {
        self.parser = nil
    }

    func testNoDataError() {
        XCTAssertThrowsPayloadParserError(try self.parser.payload(from: nil)) { (error) -> () in
            XCTAssertEqual(error, PayloadParser.Error.noData)
            XCTAssertEqual(error.localizedDescription, "Something goes wrong (#900)")
        }
    }

    func testCorruptedDataError() {
        let dataStrings = [
            "",
            "{ \"result\": true, \"payload\": []",
            "{ 'payload': { 'key': 'value' } }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                XCTAssertEqual(error, PayloadParser.Error.corruptedData)
                XCTAssertEqual(error.localizedDescription, "Something goes wrong (#901)")
            }
        }
    }

    func testNoResultError() {
        let dataStrings = [
            "{ \"payload\": [] }",
            "{ \"payload\": { \"key\": \"value\" } }",
            "{ \"message\": false }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                XCTAssertEqual(error, PayloadParser.Error.noResult)
                XCTAssertEqual(error.localizedDescription, "Something goes wrong (#902)")
            }
        }
    }

    func testUnknownResultError() {
        let dataStrings = [
            "{ \"result\": \"\", \"payload\": [] }",
            "{ \"result\": 123, \"payload\": [] }",
            "{ \"result\": 1.23, \"payload\": [] }",
            "{ \"result\": \"unknown\", \"payload\": [] }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                XCTAssertEqual(error, PayloadParser.Error.unknownResult)
                XCTAssertEqual(error.localizedDescription, "Something goes wrong (#903)")
            }
        }
    }

    func testNoPayloadError() {
        let dataStrings = [
            "{ \"result\": 1 }",
            "{ \"result\": true }",
            "{ \"result\": \"true\" }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                XCTAssertEqual(error, PayloadParser.Error.noPayload)
                XCTAssertEqual(error.localizedDescription, "Something goes wrong (#906)")
            }
        }
    }

    func testUnknownExternalError() {
        let dataStrings = [
            "{ \"result\": 0 }",
            "{ \"result\": false }",
            "{ \"result\": \"false\" }",
            "{ \"result\": false, \"message\": \"\" }",
            "{ \"result\": false, \"message\": true }",
            "{ \"result\": false, \"message\": 1 }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                XCTAssertEqual(error, PayloadParser.Error.unknownExternal)
                XCTAssertEqual(error.localizedDescription, "Something goes wrong (#905)")
            }
        }
    }

    func testExternalError() {
        let dataStrings = [
            "{ \"result\": false, \"message\": \"Example message\" }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                let message = "Example message"
                XCTAssertEqual(error, PayloadParser.Error.external(message))
                XCTAssertEqual(error.localizedDescription, message)
            }
        }
    }

    func testUnknownPayload() {
        let dataStrings = [
            "{ \"result\": true, \"payload\": 5 }",
            "{ \"result\": true, \"payload\": true }",
            "{ \"result\": true, \"payload\": {} }",
        ]

        for dataString in dataStrings {
            XCTAssertThrowsPayloadParserError(try self.parser.payload(from: self.data(from: dataString))) { (error) -> () in
                XCTAssertEqual(error, PayloadParser.Error.unknownPayload)
                XCTAssertEqual(error.localizedDescription, "Something goes wrong (#907)")
            }
        }
    }

    func testEmptyArraySuccess() {
        let dataStrings = [
            "{ \"result\": true, \"payload\": [] }",
            "{ \"result\": 1, \"payload\": [] }",
            "{ \"result\": \"true\", \"payload\": [] }",
        ]

        for dataString in dataStrings {
            let run = {
                return try self.parser.payload(from: self.data(from: dataString))
            }

            XCTAssertNoThrow(try run())
            guard let object = try! run() as? [JSONObject] else {
                return XCTFail()
            }

            XCTAssertEqual(object.count, 0)
        }
    }

    func testArraySuccess() {
        let dataStrings = [
            "{ \"result\": true, \"payload\": [ {\"key1\": \"value1\"}, {\"key2\": \"value2\" } ] }",
            "{ \"result\": 1, \"payload\": [ {\"key1\": \"value1\"}, {\"key2\": \"value2\" } ] }",
            "{ \"result\": \"true\", \"payload\": [ {\"key1\": \"value1\"}, {\"key2\": \"value2\" } ] }",
        ]

        for dataString in dataStrings {
            let run = {
                return try self.parser.payload(from: self.data(from: dataString))
            }

            XCTAssertNoThrow(try run())
            guard let object = try! run() as? [[String: String]] else {
                return XCTFail()
            }

            XCTAssertEqual(object, [ ["key1": "value1"], ["key2": "value2"] ])
        }
    }

    func testObject1Success() {
        let dataStrings = [
            "{ \"result\": true, \"payload\": { \"key\": \"value\" } }",
            "{ \"result\": 1, \"payload\": { \"key\": \"value\" } }",
            "{ \"result\": \"true\", \"payload\": { \"key\": \"value\" } }",
        ]

        for dataString in dataStrings {
            let run = {
                return try self.parser.payload(from: self.data(from: dataString))
            }

            XCTAssertNoThrow(try run())
            guard let object = try! run() as? [String: String] else {
                return XCTFail()
            }

            XCTAssertEqual(object, ["key": "value"])
        }
    }

    func testObject2Success() {
        let dataStrings = [
            "{ \"result\": true, \"payload\": { \"key\": 123 } }",
            "{ \"result\": 1, \"payload\": { \"key\": 123 } }",
            "{ \"result\": \"true\", \"payload\": { \"key\": 123 } }",
        ]

        for dataString in dataStrings {
            let run = {
                return try self.parser.payload(from: self.data(from: dataString))
            }

            XCTAssertNoThrow(try run())
            guard let object = try! run() as? [String: Int] else {
                return XCTFail()
            }

            XCTAssertEqual(object, ["key": 123])
        }
    }

    func testObject3Success() {
        let dataStrings = [
            "{ \"result\": true, \"payload\": { \"key\": { \"key\": 123 } } }",
            "{ \"result\": 1, \"payload\": { \"key\": { \"key\": 123 } } }",
            "{ \"result\": \"true\", \"payload\": { \"key\": { \"key\": 123 } } }",
        ]

        for dataString in dataStrings {
            let run = {
                return try self.parser.payload(from: self.data(from: dataString))
            }

            XCTAssertNoThrow(try run())
            guard let object = try! run() as? [String: [String: Int]] else {
                return XCTFail()
            }

            XCTAssertEqual(object, ["key": ["key": 123]])
        }
    }

}

private extension flixbus_assignmentTests {
    func data(from string: String) -> Data? {
        return string.data(using: .utf8)
    }
}
