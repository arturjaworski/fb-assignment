//
//  func.swift
//  flixbus-assignmentTests
//
//  Created by Artur Jaworski on 14/11/2018.
//  Copyright © 2018 nomtek. All rights reserved.
//

import XCTest
@testable import flixbus_assignment

func XCTAssertThrowsPayloadParserError<T>(_ expression: @autoclosure () throws -> T, _ errorHandler: (PayloadParser.Error) -> Void = { _ in }) {
    XCTAssertThrowsError(expression) { error in
        guard let error = error as? PayloadParser.Error else {
            return XCTFail("Invalid Error type. Expecting PayloadParser.Error.")
        }
        errorHandler(error)
    }
}
